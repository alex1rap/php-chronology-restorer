# PHP-ChronologyRestorer



## Using

If you want to fix last modified time of your files by their names just type:
###### On UNIX-like platforms:
```shell
./restore-chronology [...directories or files]
```
###### On all platforms:
```shell
php restore-chronology [...directories or files]
```

If you want to do it recursively just add `-R`

## Manage file formats (extensions) or masks

Just create or modify config file `cr_config.json` in your current directory or directory with this script.
